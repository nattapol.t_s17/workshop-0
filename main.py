import pandas as pd
from apyori import apriori
from sklearn import model_selection

def load_data(filename):
    data = pd.read_csv(filename)
    return data



def preprocess(data):
    item_list = data.unstack().dropna().unique()
    # print (item_list)
    # print ("# items:", len(item_list))

    train, test = model_selection.train_test_split(data, test_size=.1)
    # print ("train:", train)
    # print ("test:", test)

    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    return [train, test]
    
def model(train, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    result = list(apriori(train, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    return result

def visualize(result):
    for rule in result:
        pair = rule[0]
        component = [i for i in pair]
        print (pair)
        print ('rule:', component[0], '->', component[1])
        print ('support:', rule[1])
        print ('confidence:', rule[2][0][2])
        print ('lift:', rule[2][0][3])
        print ('-'*20)

if __name__ == "__main__":
    data = load_data('store_data.csv')
    train, test = preprocess(data)
    result = model(train)
    visualize(result)